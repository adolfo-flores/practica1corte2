package Controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelos.DBProducto;
import modelos.Productos;
import vistas.dlgManejoDatos;
import java.awt.Image;
import java.sql.Date;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.Port;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JDialog;

public class Controlador implements ActionListener {

    private dlgManejoDatos vista;
    private Productos pro;
    private DBProducto db;
    private boolean acti;
    private boolean habi;

    public Controlador(dlgManejoDatos vista, Productos pro, DBProducto db) {
        this.pro = pro;
        this.vista = vista;
        this.db = db;

        vista.btnNuevo.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnBuscarDes.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiarDes.addActionListener(this);
    }

    private void IniciarVista() {

        vista.setTitle("--:: Productos ::--");
        vista.setSize(600, 600);
        vista.setVisible(true);
        try {
            vista.tableAct.setModel(db.listar());
            vista.tableDes.setModel(db.listaD());
        } catch (Exception ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void limpiar() {
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.txtCodigoDes.setText("");
        vista.txtNombreDes.setText("");
        vista.dateFecha.setCalendar(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (vista.btnNuevo == e.getSource()) {
            vista.txtCodigoDes.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.txtCodigo.setEnabled(true);

            vista.btnGuardar.setEnabled(true);
            vista.btnBuscarDes.setEnabled(true);
            vista.btnDeshabilitar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.btnCerrar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnBuscar.setEnabled(true);
            vista.btnHabilitar.setEnabled(true);

            vista.dateFecha.setEnabled(true);
            vista.tableDes.setEnabled(true);
            vista.tableAct.setEnabled(true);
        }

        if (vista.btnBuscarDes == e.getSource()) {

            if (vista.txtCodigoDes.getText().isEmpty()) {
                JOptionPane.showMessageDialog(vista, "Seleccione el codigo a buscar...");
            } else {
                try {
                    pro = (Productos) db.buscarD(vista.txtCodigoDes.getText());
                    if (!pro.getCodigo().equals("")) {
                        habi = true;
                        vista.txtNombreDes.setText(pro.getNombre());
                        
                    } else {
                        JOptionPane.showMessageDialog(vista, "Codigo deshabilitado o INEXISTENTE");
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "ERROR al buscar desactivados..." + ex.getMessage());
                }
            }
        }

        if (vista.btnGuardar == e.getSource()) {
            try {
                pro = (Productos) db.buscar(vista.txtCodigo.getText());
                
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setNombre(vista.txtNombre.getText());
                fecha();
                db.insertar(pro);
                
                vista.tableAct.setModel(db.listar());   //actualizar :D
            } catch (Exception ex) {
                
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(vista, "ERROR al guardar" + ex.getMessage());
            }

        }
                if (vista.btnDeshabilitar == e.getSource()) {
                    if (acti != true) {
                        JOptionPane.showMessageDialog(vista, "Buscar codigo antes de deshabilitar...");
                    } else {
                        try {
                            db.deshabilitar(pro);
                            vista.tableAct.setModel(db.listar());
                            vista.tableDes.setModel(db.listaD());
                            limpiar();
                            acti = false;
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(vista, "ERROR al deshabilitar..." + ex.getMessage());
                        }
                    }
                }

                if (vista.btnHabilitar == e.getSource()) {
                    if (habi != true) {
                        JOptionPane.showMessageDialog(vista, "Buscar codigo antes de habilitar...");
                    } else {
                        try {
                            db.habilitar(pro);
                            vista.tableAct.setModel(db.listar());
                            vista.tableDes.setModel(db.listaD());
                            habi = false;
                        } catch (Exception ex) {
                            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }

                if (vista.btnCancelar == e.getSource()) {
                    limpiar();
                    vista.txtCodigoDes.setEnabled(false);
                    vista.txtNombreDes.setEnabled(false);
                    vista.txtPrecio.setEnabled(false);
                    vista.txtCodigo.setEnabled(false);

                    vista.btnGuardar.setEnabled(false);
                    vista.btnBuscarDes.setEnabled(false);
                    vista.btnDeshabilitar.setEnabled(false);
                    vista.btnCancelar.setEnabled(false);
                    vista.btnCerrar.setEnabled(false);
                    vista.btnLimpiar.setEnabled(false);
                    vista.btnBuscar.setEnabled(false);
                    vista.btnHabilitar.setEnabled(false);

                    vista.dateFecha.setEnabled(false);
                    vista.tableDes.setEnabled(false);
                    vista.tableAct.setEnabled(false);

                }

                if (vista.btnCerrar == e.getSource()) {
                    if (JOptionPane.showConfirmDialog(vista, "¿Desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION)
                            != JOptionPane.YES_NO_OPTION) {
                    } else {
                        vista.setVisible(false);
                        vista.dispose();
                        System.exit(0);
                    }
                }

                if (vista.btnLimpiar == e.getSource() || vista.btnLimpiarDes == e.getSource()) {
                    limpiar();

                    acti = false;
                }

                if (vista.btnBuscar == e.getSource()) {
                    if (vista.txtCodigo.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(vista, "Seleccione el codigo a buscar");
                    } else {
                        try {
                            pro = (Productos) db.buscar(vista.txtCodigo.getText());
                            if (!pro.getCodigo().equals("")) {
                                acti = true;
                                vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                                vista.txtNombre.setText(pro.getNombre());
                                
                                vista.dateFecha.setDate(Date.valueOf(pro.getFecha()));
                                
                            } else {
                                JOptionPane.showMessageDialog(vista, "Esta deshabilitado o no existe...");
                            }

                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(vista, "ERROR al buscar activos..." + ex.getMessage());
                        }
                    }
                }

            }

    public boolean valiVacio() {
        if (!vista.txtCodigo.getText().isEmpty() && !vista.txtNombre.getText().isEmpty()
                && !vista.txtPrecio.getText().isEmpty()
                && vista.dateFecha.getDate() != null) {
            return true;
        } else {
            return false;
        }
    }

    public void fecha() {
        int mes = vista.dateFecha.getCalendar().get(Calendar.MONTH) + 1;
        if (mes < 10) {
            pro.setFecha(String.valueOf(vista.dateFecha.getCalendar().get(Calendar.YEAR)) + "-0" + mes
                    + "-" + String.valueOf(vista.dateFecha.getCalendar().get(Calendar.DAY_OF_MONTH)));
        } else {
            pro.setFecha(String.valueOf(vista.dateFecha.getCalendar().get(Calendar.YEAR)) + "-" + mes
                    + "-" + String.valueOf(vista.dateFecha.getCalendar().get(Calendar.DAY_OF_MONTH)));
        }
    }

    public static void main(String[] args) {
        dlgManejoDatos vista = new dlgManejoDatos();
        DBProducto db = new DBProducto();
        Productos pro = new Productos();

        Controlador contr = new Controlador(vista, pro, db);
        contr.IniciarVista();

    }
}