package modelos;

import java.sql.Date;

public class TestDB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Productos pro = new Productos();
        DBProducto db = new DBProducto();

        pro.setCodigo("10010");
        pro.setFecha("2023-6-21");
        pro.setNombre("Avena");
        pro.setPrecio(34.60f);
        pro.setStatus(0);

        try {
            db.insertar(pro);
            System.out.println("Se agrego con exito");
            
            Productos res = new Productos();
            res = (Productos) db.buscar("10010");
            System.out.println("Nombre: " + res.getNombre() + "Precio: " + res.getPrecio());

            
        } catch (Exception e) {
            System.err.println("Surgio un error" + e.getMessage());
        }
    }
}
