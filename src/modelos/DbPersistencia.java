package modelos;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;


public interface DbPersistencia {
    
    public void insertar(Object objecto) throws Exception;
    public void actualizar(Object objecto) throws Exception;
    public void habilitar(Object objecto) throws Exception;
    public void deshabilitar(Object objecto) throws Exception;
    
    public boolean isExiste(int id) throws Exception;
    public DefaultTableModel listaD() throws Exception;
    public DefaultTableModel listar() throws Exception;
    
    public Object buscarD(String codigo) throws Exception;
    public Object buscar(String codigo) throws Exception;
}
